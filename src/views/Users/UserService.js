const userService = {
  userList: [
    { id: 1, name: 'Thinnait', gender: 'M' },
    { id: 2, name: 'Prasob', gender: 'M' }
  ],
  lastId: 3,
  addUser(user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser(user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  delereUser(user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers() {

    return [...this.userList]

  }
}
export default userService